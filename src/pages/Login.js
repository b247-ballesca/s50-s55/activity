import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { useNavigate, Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login(){

	// Allows us to consume the User Context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);

	// hook return a function that lets you navigate to components.
	//const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState("true")

	function authenticate(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			} else {
				Swal.fire({
					title: "Authentication Failed!",
					icon: 'error',
					text: 'Please, check you login details and try again!'
				})
			}
		});

		// Set the email of the authenticated user in the local storage
			// Syntax
			// localStorage.setItem('propertyName', value)
		//localStorage.setItem("email", email);

		// In this case, the key name is 'email' and the value is the value of the email variable. This code sets the value of 'email' key in the local storage to the value of the email variable.

		//setUser( { email: localStorage.getItem('email')} );

		setEmail("");
		setPassword("");
		//navigate('/');

		//alert(`"${email}" has been verified! Welcome back!`);
	};

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	};



	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password]);

	return (
		(user.id !== null) ? 
			<Navigate to="/courses" />
		:

		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			{ isActive ? 

				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>

				: 

				<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>

			}
			
		</Form>
	)
}