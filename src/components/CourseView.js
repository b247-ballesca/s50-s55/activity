import { useParams, useNavigate, Link } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import UserContext from '../UserContext';

import Swal from 'sweetalert2'

export default function CouseView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const {courseId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0); 
	const [enrollees, setEnrollees] = useState(0);

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})
				navigate("/courses");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}



	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/details`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setEnrollees(data.enrollees.length)
		})
	}, [courseId])

	return (
		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>Class Schedule:</Card.Subtitle>
					        <Card.Text>8:00 AM - 5:00 PM</Card.Text>
					        
					        {
					        	(user.id !== null) ?
					        		<>
					        		<Card.Subtitle>Current Number of Enrollees:</Card.Subtitle>
					        		<Card.Text>{enrollees}</Card.Text>
					        		<Button variant="primary" onClick={() => enroll(courseId)} >Enroll</Button>
					        		</>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login">Log in to Enroll</Button>
					        }


					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)
};